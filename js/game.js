var game = new Phaser.Game(400, 400, Phaser.AUTO, 'Phaser-Ex');
game.global = {
  score: 0,
  level: 0,
  state: 'idle'
};
game.state.add('boot', bootState);
game.state.add('loading', loadingState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('gameover', gameoverState);
game.state.start('boot');
