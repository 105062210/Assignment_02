var gameoverState = {
  preload: function () {

  },
  create: function() {
    var nameLabel = game.add.text(game.width/2, 80, 'GAME OVER', { font: '50px Arial', fill: '#ff0000' });
    nameLabel.anchor.setTo(0.5, 0.5);
    // Show the score at the center of the screen
    var scoreLabel = game.add.text(game.width/2, game.height/2, 'score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
    scoreLabel.anchor.setTo(0.5, 0.5);
    var scoreLabel = game.add.text(game.width/2 - 5, game.height/2 + 30, 'level: ' + game.global.level, { font: '25px Arial', fill: '#ffffff' });
    scoreLabel.anchor.setTo(0.5, 0.5);
    // Explain how to start the game
    var startLabel = game.add.text(game.width/2, game.height-80, 'press the up arrow to restart', { font: '25px Arial', fill: '#ffffff' });
    startLabel.anchor.setTo(0.5, 0.5);
    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    upKey.onDown.add(this.restart, this);
  },
  restart: function() {
    console.log('Restart!');
    game.global.score = 0;
    game.global.level = 0;
    game.global.state = 'play';
    game.state.start('play');
  }
};
