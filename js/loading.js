var loadingState = {
  preload: function () {
    // Add a 'loading...' label on the screen
    var loadingLabel = game.add.text(game.width/2, 150,'loading...', { font: '30px Arial', fill: '#ffffff' });
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
    var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
    progressBar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(progressBar);

  },
  create: function() {
    // Go to the menu state
    game.state.start('menu');
  }
};
