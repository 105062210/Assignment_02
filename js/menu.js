var menuState = {
  preload: function() {
    game.load.image("background", "assets/hellDoor.jpg");
  },
  create: function() {
    // Add a background image
    var background = game.add.image(0, 0, 'background');
    background.height = game.height;
    background.width = game.width;
    background.smoothed = false;
    background.alpha = 0.85;

    // Display the name of the game
    var nameLabel = game.add.text(game.width/2, 80, '小朋友下地獄', { font: '50px Arial', fill: '#ffffff' });
    nameLabel.anchor.setTo(0.5, 0.5);
    // Show the score at the center of the screen
    var scoreLabel = game.add.text(game.width/2, game.height/2, 'score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
    scoreLabel.anchor.setTo(0.5, 0.5);
    var scoreLabel = game.add.text(game.width/2 - 5, game.height/2 + 30, 'level: ' + game.global.level, { font: '25px Arial', fill: '#ffffff' });
    scoreLabel.anchor.setTo(0.5, 0.5);
    // Explain how to start the game
    var startLabel = game.add.text(game.width/2, game.height-80, 'press the up arrow to start', { font: '25px Arial', fill: '#ffffff' });
    startLabel.anchor.setTo(0.5, 0.5);

    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    upKey.onDown.add(this.start, this);
  },
  start: function() {
    // Start the actual game
    game.state.start('play');
  }
};
