
var playState = {
  preload: preload,
  create: create,
  update: update
};
var timeInterval = 3600;
var lastTime = 0;
var walls = [];
var topNails = [];
var platforms = [];
var scoreText = null;
var levelText = null;
var hpText = null;
var rollSpeed = 0.75;
var currentLevel = 0;
var lastCountLevel = 0;
var music = null;
var playerInfo = {
  HP: 0,
  standOn: null,
  unbeatableTime: 0
};

// Produce a random number in range (min, max)
function randomInRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

// Produce a random number from the specified probability distribution
function randomChooseFromDistribution(...probs) {
  let num = Math.random();
  let s = 0;
  let lastIndex = probs.length - 1;
  let results = [];

  for (var i = 0; i < probs.length; i++) {
    results.push(i + 1);
  }

  for (var i = 0; i < lastIndex; ++i) {
      s += probs[i];
      if (num < s) {
          return results[i];
      }
  }

  return results[lastIndex];
}

function generateItem(x, y, type, level) {
  var brick = game.add.sprite(x, y, type);
  if (type === 'conveyor_left' || type === 'conveyor_right') {
    //brick.frame = 8;
    brick.animations.add('scroll', [0, 1, 2, 3], 16, true);
    brick.animations.play('scroll');
  }
  game.physics.arcade.enable(brick);
  brick.body.immovable = true;
  brick.key += level.toString();
  platforms.push(brick);
}

function generateType() {
  var type = '';
  switch (randomChooseFromDistribution(0.6, 0.2, 0.1, 0.1)) {
    case 1:
      type = 'brick';
      break;
    case 2:
      type = 'nails';
      break;
    case 3:
      type = 'conveyor_left';
      break;
    case 4:
      type = 'conveyor_right';
      break;
    default:
      type = 'brick';
  }
  return type;
}

function generateBricks(x, y, numOfBrick, level) {
  if (numOfBrick === 1) // Create one brick on current level
    if (game.global.state === 'idle') {
      generateItem(x, y, 'brick', level);
      game.global.state = 'play';
    } else {
      generateItem(x, y, generateType(), level);
    }
  else { // Create two brick on current level
      // Produce the first brick
      generateItem(x, y, generateType(), level);

      // Try to produce the secondary brick
      let leftLeftSpace = x - 15;
      let rightLeftSpace = 385 - (x + 96);

      if (rightLeftSpace > 96 || leftLeftSpace > 96) { // Check that is there any space avaliable
        if (rightLeftSpace > 96 && leftLeftSpace > 96) { // Left space and right space are avaliable
          if (randomChooseFromDistribution(0.5, 0.5) === 1) { // Randomly choose one space to produce the secondary brick
            x = randomInRange(x + 96, 290 - 2);
            generateItem(x, y, generateType(), level);
          } else {
            x = randomInRange(15, x - 96);
            generateItem(x, y, generateType(), level);
          }
        } else {
          if (rightLeftSpace > 96) { // Only right space is avaliable
            x = randomInRange(x + 96, 290 - 2);
            generateItem(x, y, generateType(), level);
          }
          else if (leftLeftSpace > 96) { // Only left space is avaliable
            x = randomInRange(15, x - 96);
            generateItem(x, y, generateType(), level);
          }
        }
      }
  }
}

// Initial the game world
function initWorld () {

  // Walls (15 * 32) init
  for (var i = 0; i < 13; i++) {
    var temp1 = game.add.sprite(0, 0 + 32 * i, 'wall');
    var temp2 = game.add.sprite(385, 0 + 32 * i, 'wall');
    game.physics.arcade.enable(temp1);
    game.physics.arcade.enable(temp2);
    temp1.body.immovable = true;
    temp2.body.immovable = true;
    walls.push(temp1);
    walls.push(temp2);
  }

  // Top nails init
  for (var i = 0; i < 13; i++) {
    var nails = game.add.sprite(0 + 32 * i, 16, 'nails');
    nails.scale.y *= -1;
    game.physics.arcade.enable(nails);
    nails.body.immovable = true;
    topNails.push(nails);
  }

  // Bricks (96 * 15) init
  let brickPositionX = 145;
  let brickPositionY = 130;
  let brickNumInOneLevel = 1;
  var i = 0, j = 0;

  for (i = 0; i < 5; i++) {
    generateBricks(brickPositionX, brickPositionY, brickNumInOneLevel, currentLevel++);
    brickNumInOneLevel = randomChooseFromDistribution(0.4, 0.6);
    brickPositionX = randomInRange(15, 290);
    brickPositionY += randomInRange(65, 90);
  }

}

function effect(player, platform) {
  playerInfo.standOn = platform;

  if (platform.key.indexOf('nails') !== -1 && playerInfo.unbeatableTime === 0) {
    console.log('hit');
    playerInfo.HP -= 1;
    hpText.text = "HP " + playerInfo.HP.toString();
    playerInfo.unbeatableTime = 300;
  }

  // Level affect and score compute
  if (platform.key.indexOf('brick') !== -1 || platform.key.indexOf('nails') !== -1) {
    game.global.level = parseInt(platform.key.slice(5, platform.key.length));
  }
  else if (platform.key.indexOf('conveyor_left') !== -1) {
    game.global.level = parseInt(platform.key.slice(13, platform.key.length));
    player.body.x -= 1;
  }
  else if (platform.key.indexOf('conveyor_right') !== -1) {
    game.global.level = parseInt(platform.key.slice(14, platform.key.length));
    player.body.x += 1;
  }
  levelText.text = "Level " + game.global.level.toString();

  if (game.global.level !== 0) {
    if (game.global.level % 5 === 0 && lastCountLevel !== game.global.level) {
      lastCountLevel = game.global.level;
      game.global.score += 100;
      scoreText.text = "Score : " + game.global.score.toString();
    }
    if (game.global.level % 10 === 0 && lastCountLevel !== game.global.level) {
      lastCountLevel = game.global.level;
      game.score += 1000;
      scoreText.text = "Score : " + game.global.score.toString();
    }
  }
}

function topEffect(player, platform) {
  if (platform.key.indexOf('nails') !== -1 && playerInfo.unbeatableTime === 0) {
    console.log('hit');
    playerInfo.HP -= 1;
    hpText.text = "HP " + playerInfo.HP.toString();
    playerInfo.unbeatableTime = 300;
  }
}

// (Preload) - Phaser
function preload () {
  console.log('Loading assets...');
  // Load the image from assets
  game.load.crossOrigin = 'anonymous'; // Set the crossorigin to avoid the CORS
  // game.load.baseURL = 'http://s.ntustcoding.club/ns-shaft-workshop/';
  // game.load.baseURL = 'file:///Users/superuser/Desktop/Eating/phaser/assets/'; // Set the loading directory to assets
  game.load.spritesheet('player', 'assets/player.png', 32, 32);
  game.load.image('wall', 'assets/wall.png');
  game.load.image('brick', 'assets/brick.png');
  game.load.image('nails', 'assets/nails.png');
  game.load.spritesheet('conveyor_right', 'assets/conveyor_right.png', 96, 16);
  game.load.spritesheet('conveyor_left', 'assets/conveyor_left.png', 96, 16);
  game.load.image('button', 'assets/button.png');
  game.load.image("background", "assets/hell.png");

  game.load.audio('bgm', 'assets/bgm.mp3');
}

// (Create) - Phaser
function create() {
  music = game.add.audio('bgm');
  music.play();

  var background = game.add.image(0, 0, 'background');
  background.height = game.height;
  background.width = game.width;
  background.smoothed = false;
  background.alpha = 0.85;

  // Load player's sprite
  player = game.add.sprite(180, 50, 'player');
  player.anchor.setTo(0.5, 0.5);
  // Enable player's physics arcade
  game.physics.arcade.enable(player);

  // Set the player's initial position
  var x = player.body.x;
  var y = player.body.y;

  // Physics config
  player.body.gravity.y = 300;

  // Animaiton config
  player.frame = 8;
  player.animations.add('idle', [8], 6);
  player.animations.add('left', [0, 1, 2, 3], 6);
  player.animations.add('right', [9, 10, 11, 12], 6);
  player.animations.add('jump_front', [36, 37, 38, 39], 6);
  player.animations.add('jump_right', [27, 28, 29, 30], 6);
  player.animations.add('jump_left', [18, 19, 20, 21], 6);

  // Initialization for game world
  console.log('Initializing the world...');
  initWorld();

  // Initialize the player's information
  playerInfo.HP = 5;
  playerInfo.standOn = '';
  playerInfo.unbeatableTime = 0;

  // Initialize UI
  var style = { font: "20px Arial", fill: "#ffffff"};
  scoreText = game.add.text(20, 8, 'Score : 0', style);
  scoreText.addColor('#ffffff', 0);

  levelText = game.add.text(300, 8, 'Level 0', style);
  levelText.addColor('#ffbbdd', 0);

  hpText = game.add.text(320, 28, 'HP ' + playerInfo.HP.toString(), style);
  hpText.addColor('#ff2211', 0);

  // Keyboard config
  keyboard = game.input.keyboard.addKeys({
      'up': Phaser.Keyboard.UP,
      'down': Phaser.Keyboard.DOWN,
      'left': Phaser.Keyboard.LEFT,
      'right': Phaser.Keyboard.RIGHT
  });

  lastTime = game.time.now;
}

// (Update) - Phaser
function update() {

  if (player.body.y > 420 || playerInfo.HP === 0) {
    timeInterval = 3600;
    lastTime = 0;
    for(var i = 0; i < walls.length; i++)
      walls[i].destroy();
    walls = [];
    for(var j = 0; j < platforms.length; j++)
      platforms[j].destroy();
    platforms = [];
    player.destroy();
    scoreText = null;
    levelText = null;
    rollSpeed = 0.75;
    currentLevel = 0;
    lastCountLevel = 0;
    game.global.state = 'idle';
    music.pause();
    music = null;
    game.state.start('gameover');
    return;
  }

  // HP affect
  if (playerInfo.unbeatableTime !== 0) {
    playerInfo.unbeatableTime--;
  }

  // Bricks move
  platforms.forEach((platform, key) => {
    game.physics.arcade.collide(player, platform, effect);
    if (game.global.state === 'idle') return;
    if (platform.body.y < -20) {
      platform.destroy();
      platforms.splice(key, 1);
    }
    else {
      platform.body.y -= rollSpeed;
    }
  });

  if (game.time.now - lastTime > timeInterval) {
    generateBricks(randomInRange(15, 290), 420, randomChooseFromDistribution(0.4, 0.6), currentLevel++);
    lastTime = game.time.now;
  }

  if (keyboard.left.isDown)
  {
    player.animations.play('left');
    player.body.x -= 2;
  }
  else if (keyboard.right.isDown)
  {
    player.animations.play('right');
    player.body.x += 2;
  } else {
    player.animations.play('idle');
  }

  if (keyboard.up.isDown)
  {
    if (keyboard.right.isDown)
      player.animations.play('jump_right');
    else if (keyboard.left.isDown)
      player.animations.play('jump_left');
    else
      player.animations.play('jump_front');
    player.body.y -= 3.5;
  }


  game.physics.arcade.collide(player, walls);
  game.physics.arcade.collide(player, topNails, topEffect);
}
